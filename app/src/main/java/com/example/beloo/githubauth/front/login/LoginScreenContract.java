package com.example.beloo.githubauth.front.login;

import com.example.beloo.githubauth.front.IPresenter;
import com.example.beloo.githubauth.front.IView;

public interface LoginScreenContract {

    interface LoginView extends IView {
        void openOauthScreen();
        void openRepositoriesScreen();
    }

    interface LoginPresenter extends IPresenter<LoginView> {
        void onOauthCompleted();
        void onLoginClicked();
    }

}
