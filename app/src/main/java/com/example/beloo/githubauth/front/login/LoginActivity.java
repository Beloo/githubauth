package com.example.beloo.githubauth.front.login;

import android.content.Intent;
import android.os.Bundle;

import com.example.beloo.githubauth.R;
import com.example.beloo.githubauth.front.ViewActivity;
import com.example.beloo.githubauth.front.main.MainActivity;
import com.example.beloo.githubauth.front.oauth.OAuthActivity;

import javax.inject.Inject;

import butterknife.OnClick;

public class LoginActivity extends ViewActivity implements LoginScreenContract.LoginView {
    private static final int REQUEST_CODE_OAUTH = 10;

    @Inject
    LoginScreenContract.LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_login);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onComponentsLoaded() {
        getPresenterComponent().inject(this);
        presenter.bindView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.unbindView();
    }

    @OnClick(R.id.btn_login)
    void onLoginClicked() {
        presenter.onLoginClicked();
    }

    @Override
    public void openOauthScreen() {
        startActivityForResult(new Intent(this, OAuthActivity.class), REQUEST_CODE_OAUTH);
    }

    @Override
    public void openRepositoriesScreen() {
        MainActivity.start(this);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_OAUTH:
                presenter.onOauthCompleted();
                break;
        }
    }
}
