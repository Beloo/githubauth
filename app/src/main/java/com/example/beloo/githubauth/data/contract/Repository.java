package com.example.beloo.githubauth.data.contract;

import java.util.Date;

public interface Repository {
    Integer getId();

    String getName();

    Integer getStargazersCount();

    Date getUpdatedAt();
}
