package com.example.beloo.githubauth.front.login;

import android.util.Log;

import com.example.beloo.githubauth.data.local.UserDataSource;
import com.example.beloo.githubauth.front.AbstractPresenter;

import javax.inject.Inject;

public class LoginPresenter extends AbstractPresenter<LoginScreenContract.LoginView> implements LoginScreenContract.LoginPresenter {

    private static final String TAG = LoginPresenter.class.getSimpleName();
    @Inject
    UserDataSource userDataSource;

    @Inject
    LoginPresenter(){}

    @Override
    protected void onReady() {
        if (userDataSource.isUserLoggedIn()) {
            getView().openRepositoriesScreen();
        }
    }


    @Override
    public void onOauthCompleted() {
        if (!isViewAttached()) return;
        if (userDataSource.isUserLoggedIn()) {
            Log.d(TAG, "on oauth successful");
            getView().openRepositoriesScreen();
        }
    }

    @Override
    public void onLoginClicked() {
        Log.d(TAG, "open oauth screen");
        getView().openOauthScreen();
    }
}
