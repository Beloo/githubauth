package com.example.beloo.githubauth.front.oauth;

import com.example.beloo.githubauth.front.IPresenter;
import com.example.beloo.githubauth.front.IProgressView;
import com.example.beloo.githubauth.front.IView;

import rx.Observable;

public interface OauthScreenContract {

    interface OAuthView extends IView, IProgressView {
        Observable<String> loadUrl(String url);
        void onLoginSuccessful();
        void onLoginCancelled();
        void stopLoading();
    }

    interface OAuthPresenter extends IPresenter<OAuthView> {}

}
