package com.example.beloo.githubauth.injection;

import android.content.Context;

public class ComponentCreator implements ComponentFactory {

    private Context appContext;

    public ComponentCreator(Context appContext) {
        this.appContext = appContext;
    }

    @Override
    public GlobalComponent globalComponent() {
        return DaggerGlobalComponent.builder()
                .globalModule(new GlobalModule(appContext))
                .build();
    }

    @Override
    public PresenterComponent presenterComponent() {
        return DaggerPresenterComponent.builder()
                .globalComponent(globalComponent())
                .build();
    }

    @Override
    public Context getAppContext() {
        return appContext;
    }
}
