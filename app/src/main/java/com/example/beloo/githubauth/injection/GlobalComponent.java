package com.example.beloo.githubauth.injection;

import com.example.beloo.githubauth.data.local.LocalDataSourceModule;
import com.example.beloo.githubauth.data.local.PreferenceModule;
import com.example.beloo.githubauth.data.local.UserDataSource;
import com.example.beloo.githubauth.data.network.JsonConverterModule;
import com.example.beloo.githubauth.data.network.NetworkApiModule;
import com.example.beloo.githubauth.data.network.source.AuthSource;
import com.example.beloo.githubauth.data.network.source.RemoteDataSourceModule;
import com.example.beloo.githubauth.data.network.source.RepositorySource;
import com.example.beloo.githubauth.front.ViewActivity;
import com.example.beloo.githubauth.front.ViewFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        GlobalModule.class,
        PreferenceModule.class,
        LocalDataSourceModule.class,
        NetworkApiModule.class,
        JsonConverterModule.class,
        RemoteDataSourceModule.class
})
public interface GlobalComponent {
    IPresenterComponentProvider presenterComponentProvider();
    AuthSource authSource();
    RepositorySource repositorySource();
    UserDataSource userDataSource();

    void inject(ViewActivity viewActivity);
    void inject(ViewFragment viewFragment);
}
