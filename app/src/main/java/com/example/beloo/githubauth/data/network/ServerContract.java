package com.example.beloo.githubauth.data.network;

import com.example.beloo.githubauth.BuildConfig;

import java.util.Locale;

public final class ServerContract {

    private ServerContract(){}

    public static final class Repository {

        /** get repositories list */
        public static final String GET_REPOSITORIES = "user/repos";
    }

    public static final class Auth {

        public static final String REGISTERED_REDIRECT_URL = "http://redirected";
        private static final String GET_CODE_FOR_OAUTH_TOKEN = BuildConfig.GITHUB_ENDPOINT + "login/oauth/authorize?client_id=%s&redirect_uri=%s&state=%s";
        /** parameter received after first step of OAuth*/
        public static final String CODE = "code";
        public static final String POST_OAUTH_TOKEN = "login/oauth/access_token";

        public static String getOauthToken() {
            return String.format(Locale.getDefault(), GET_CODE_FOR_OAUTH_TOKEN, BuildConfig.CLIENT_ID, REGISTERED_REDIRECT_URL, "wtf");
        }
    }

    static final class Headers {
        static final String CONTENT_TYPE = "Content-Type";
        static final String ACCEPT = "Accept";
        static final String APPLICATION_JSON = "application/json";
    }

    public static class NetworkingConstants {

        public static final String SERVER_DATE_FORMAT = "dd.MM.yyyy";
        public static final String SERVER_MONTH_FORMAT = "MM.yyyy";
    }
}
