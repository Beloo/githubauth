package com.example.beloo.githubauth.data.network.source;

import com.example.beloo.githubauth.data.contract.Repository;

import java.util.List;

import rx.Observable;

public interface RepositorySource {
    Observable<List<Repository>> getRepositories();
}
