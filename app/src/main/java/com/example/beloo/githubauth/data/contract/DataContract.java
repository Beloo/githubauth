package com.example.beloo.githubauth.data.contract;

public interface DataContract {

    interface SharedPrefs {
        String PREFS_USER_DATA = "userData";
    }

    interface Loader {
        int PRESENTER_COMPONENT_LOADER_ID = 101;
    }
}
