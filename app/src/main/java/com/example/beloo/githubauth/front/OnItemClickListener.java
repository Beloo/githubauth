package com.example.beloo.githubauth.front;

public interface OnItemClickListener {
    void onItemClicked(int position);
}
