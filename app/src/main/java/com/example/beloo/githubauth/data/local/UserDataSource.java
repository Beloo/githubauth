package com.example.beloo.githubauth.data.local;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.beloo.githubauth.data.contract.AccessToken;

public interface UserDataSource {
    void storeAccessToken(@NonNull AccessToken accessToken);

    @Nullable
    AccessToken getAccessTokenHolder();

    boolean isUserLoggedIn();

    void clear();
}
