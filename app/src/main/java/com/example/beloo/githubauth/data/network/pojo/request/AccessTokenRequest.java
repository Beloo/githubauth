package com.example.beloo.githubauth.data.network.pojo.request;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccessTokenRequest {

    @JsonProperty("client_id")
    @NonNull
    private String clientId;

    @JsonProperty("client_secret")
    @NonNull
    private String clientSecret;

    @JsonProperty("code")
    @NonNull
    private String code;

    public AccessTokenRequest(@NonNull String clientId, @NonNull String clientSecret, @NonNull String code) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.code = code;
    }

    @NonNull
    public String getClientId() {
        return clientId;
    }

    @NonNull
    public String getClientSecret() {
        return clientSecret;
    }

    @NonNull
    public String getCode() {
        return code;
    }
}