package com.example.beloo.githubauth.injection;

import com.example.beloo.githubauth.front.PresenterModule;
import com.example.beloo.githubauth.front.login.LoginActivity;
import com.example.beloo.githubauth.front.main.RepositoriesFragment;
import com.example.beloo.githubauth.front.oauth.OAuthActivity;

import dagger.Component;

@PresenterScope
@Component(dependencies = {GlobalComponent.class}, modules = {PresenterModule.class})
public interface PresenterComponent extends GlobalComponent {

    void inject(OAuthActivity oAuthActivity);
    void inject(LoginActivity loginActivity);
    void inject(RepositoriesFragment fragment);
}
