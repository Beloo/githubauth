package com.example.beloo.githubauth.data.network.source;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RemoteDataSourceModule {

    @Provides
    AuthSource provideAuthSource(AuthRemoteSource authRemoteSource) {
        return authRemoteSource;
    }

    @Provides
    RepositorySource provideRepositorySource(RepositoryRemoteSource repositoryRemoteSource) {
        return repositoryRemoteSource;
    }

}
