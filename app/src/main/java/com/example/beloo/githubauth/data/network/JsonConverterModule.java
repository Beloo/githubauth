package com.example.beloo.githubauth.data.network;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import dagger.Module;
import dagger.Provides;
import retrofit2.Converter;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Module
public class JsonConverterModule {

    @Provides
    ObjectMapper provideObjectMapper() {
        final SimpleModule module = new SimpleModule("", Version.unknownVersion());

        //jackson object mapper setup
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(module);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return objectMapper;
    }

    @Provides
    Converter.Factory provideJacksonConverterFactory(ObjectMapper objectMapper) {
        return JacksonConverterFactory.create(objectMapper);
    }

}
