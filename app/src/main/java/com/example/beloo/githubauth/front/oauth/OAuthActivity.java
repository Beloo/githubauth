package com.example.beloo.githubauth.front.oauth;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.example.beloo.githubauth.R;
import com.example.beloo.githubauth.front.ViewActivity;

import javax.inject.Inject;

import butterknife.BindView;
import rx.Observable;
import rx.subjects.PublishSubject;

public class OAuthActivity extends ViewActivity implements OauthScreenContract.OAuthView {
    @BindView(R.id.webView)
    WebView webView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Inject
    OauthScreenContract.OAuthPresenter presenter;

    PublishSubject<String> publishSubject = PublishSubject.create();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_oauth);
        super.onCreate(savedInstanceState);

        initWebView();
    }

    private void initWebView() {
        WebSettings webSettings = webView.getSettings();
        webSettings.setAppCachePath(getFilesDir().getPath()+"/cache");
        webSettings.setAppCacheEnabled(true);
        webSettings.setJavaScriptEnabled(true);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                publishSubject.onNext(url);
            }
        });

    }

    @Override
    public void onComponentsLoaded() {
        getPresenterComponent().inject(this);
        presenter.bindView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.unbindView();
    }

    @Override
    public Observable<String> loadUrl(String url) {
        webView.loadUrl(url);
        return publishSubject;
    }

    @Override
    public void onLoginSuccessful() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onLoginCancelled() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void stopLoading() {

    }

    @Override
    public void showProgress() {
        webView.stopLoading();
        webView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }
}
