package com.example.beloo.githubauth.injection;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;

import com.example.beloo.githubauth.data.contract.DataContract;
import com.example.beloo.githubauth.front.PresenterComponentLoader;
import com.example.beloo.githubauth.support.Consumer;

import javax.inject.Inject;

public class PresenterComponentProvider implements IPresenterComponentProvider, LoaderManager.LoaderCallbacks<PresenterComponent> {

    private Consumer<PresenterComponent> presenterConsumer;

    @Inject
    Context context;

    @Inject
    PresenterComponentProvider() {}

    @Override
    public void init(LoaderManager loaderManager, @NonNull Consumer<PresenterComponent> presenterConsumer) {
        this.presenterConsumer = presenterConsumer;
        //loaders should be called in onActivityCreated due to google guidelines
        loaderManager.initLoader(DataContract.Loader.PRESENTER_COMPONENT_LOADER_ID, null, this);
    }

    @Override
    public android.support.v4.content.Loader onCreateLoader(int id, Bundle args) {
        return new PresenterComponentLoader(context);
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader loader, PresenterComponent data) {
        loader.stopLoading();
        presenterConsumer.accept(data);
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader loader) {
    }
}
