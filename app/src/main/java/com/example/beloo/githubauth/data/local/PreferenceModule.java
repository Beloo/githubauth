package com.example.beloo.githubauth.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.beloo.githubauth.data.contract.DataContract;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PreferenceModule {

    @Provides
    @Singleton
    @Named(DataContract.SharedPrefs.PREFS_USER_DATA)
    SharedPreferences provideUserPrefs(Context context) {
        return context.getSharedPreferences(DataContract.SharedPrefs.PREFS_USER_DATA, Context.MODE_PRIVATE);
    }

}
