package com.example.beloo.githubauth.front;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.beloo.githubauth.GithubAuthApplication;
import com.example.beloo.githubauth.R;
import com.example.beloo.githubauth.injection.IPresenterComponentProvider;
import com.example.beloo.githubauth.injection.PresenterComponent;

import javax.inject.Inject;

import butterknife.ButterKnife;


@SuppressLint("Registered")
public abstract class ViewActivity extends AppCompatActivity implements IView {

    private PresenterComponent presenterComponent;

    @Inject
    IPresenterComponentProvider provider;

    public ViewActivity() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        GithubAuthApplication.instance()
                .componentFactory()
                .globalComponent()
                .inject(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        provider.init(getSupportLoaderManager(), this::onPresenterComponentLoaded);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenterComponent = null;
    }

    private void onPresenterComponentLoaded(PresenterComponent component) {
        presenterComponent = component;
        onComponentsLoaded();
    }

    public abstract void onComponentsLoaded();

    /**
     * retrieve presenter component and you'll able to inject presenters.
     * this method should be called in {@link Fragment#onResume()} ()} or in {@link Activity#onStart()}, because
     * presenter component loader is working on a main thread and it is ready in that methods.
     * Fragment have a broken loader lifecycle, so getComponent should be called in onResume
     */
    public PresenterComponent getPresenterComponent() {
        return presenterComponent;
    }

    @IdRes
    public int getPrimaryContainerId() {
        return R.id.container;
    }

    @Override
    public FragmentActivity getActivity() {
        return this;
    }

    @Override
    public void showError(Throwable throwable) {
        Toast.makeText(this, throwable.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(@StringRes int stringResource) {
        Toast.makeText(this, stringResource, Toast.LENGTH_SHORT).show();
    }
}
