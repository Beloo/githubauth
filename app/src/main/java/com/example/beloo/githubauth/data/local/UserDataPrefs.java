package com.example.beloo.githubauth.data.local;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.example.beloo.githubauth.data.contract.AccessToken;
import com.example.beloo.githubauth.data.contract.DataContract;
import com.example.beloo.githubauth.data.network.pojo.AccessTokenFactory;

import javax.inject.Inject;
import javax.inject.Named;

class UserDataPrefs implements UserDataSource {
    private static final String KEY_TOKEN = "token";
    private static final String KEY_SCOPE = "scope";
    private static final String KEY_TOKEN_TYPE = "tokenType";

    @Inject
    @Named(DataContract.SharedPrefs.PREFS_USER_DATA)
    SharedPreferences prefs;

    @Inject
    AccessTokenFactory accessTokenFactory;

    private AccessToken accessToken;

    @Inject
    UserDataPrefs() {}

    @Override
    public void storeAccessToken(@NonNull AccessToken accessToken) {

        if (TextUtils.isEmpty(accessToken.getAccessToken()))
            throw new IllegalArgumentException("bad token");

        prefs.edit()
                .putString(KEY_TOKEN, accessToken.getAccessToken())
                .putString(KEY_SCOPE, accessToken.getScope())
                .putString(KEY_TOKEN_TYPE, accessToken.getTokenType())
                .apply();
    }

    @Nullable
    @Override
    public AccessToken getAccessTokenHolder() {
        if (accessToken == null) {
            String token = prefs.getString(KEY_TOKEN, "");
            String scope = prefs.getString(KEY_SCOPE, "");
            String tokenType = prefs.getString(KEY_TOKEN_TYPE, "");

            if (!TextUtils.isEmpty(token)) {
                accessToken = accessTokenFactory.buildAccessToken(token, scope, tokenType);
            }
        }
        return accessToken;
    }

    @Override
    public boolean isUserLoggedIn() {
        return getAccessTokenHolder() != null;
    }

    @Override
    public void clear() {
        prefs.edit()
                .clear()
                .apply();
    }
}
