package com.example.beloo.githubauth.data.network.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.example.beloo.githubauth.data.contract.AccessToken;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "access_token",
        "scope",
        "token_type"
})
public class AccessTokenPoJo implements AccessToken {

    @JsonProperty("access_token")
    private String accessToken;
    @JsonProperty("scope")
    private String scope;
    @JsonProperty("token_type")
    private String tokenType;

    public AccessTokenPoJo() {}

    AccessTokenPoJo(String accessToken, String scope, String tokenType) {
        this.accessToken = accessToken;
        this.scope = scope;
        this.tokenType = tokenType;
    }

    @Override
    public String getAccessToken() {
        return accessToken;
    }

    @Override
    public String getScope() {
        return scope;
    }

    @Override
    public String getTokenType() {
        return tokenType;
    }


}