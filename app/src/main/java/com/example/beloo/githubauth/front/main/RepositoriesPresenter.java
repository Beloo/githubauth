package com.example.beloo.githubauth.front.main;

import com.example.beloo.githubauth.data.contract.Repository;
import com.example.beloo.githubauth.data.network.source.RepositorySource;
import com.example.beloo.githubauth.front.AbstractPresenter;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;

public class RepositoriesPresenter extends AbstractPresenter<RepositoriesScreenContract.RepositoriesView> implements RepositoriesScreenContract.RepositoriesPresenter {

    @Inject
    RepositorySource repositorySource;

    private Observable<List<Repository>> dataObservable;

    @Inject
    RepositoriesPresenter(){}

    @Override
    protected void onReady() {
        if (dataObservable == null)
            dataObservable = repositorySource.getRepositories().cache();

        Subscription subscription = dataObservable.subscribe(getView()::onDataLoaded, getView()::showError);
        registerSubscription(subscription);
    }
}
