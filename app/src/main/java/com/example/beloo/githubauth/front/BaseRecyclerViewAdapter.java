package com.example.beloo.githubauth.front;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Base adapter for recycler view
 */
public abstract class BaseRecyclerViewAdapter<TData>
        extends RecyclerView.Adapter<AbstractViewHolder<TData>> {

    private Context context;
    private final List<TData> data;

    protected BaseRecyclerViewAdapter(@NonNull final Context context) {
        this.context = context.getApplicationContext();
        data = new ArrayList<>();
    }

    public BaseRecyclerViewAdapter(@NonNull final Context context, @NonNull List<TData> data) {
        this.context = context.getApplicationContext();
        this.data = new ArrayList<>(data);
    }

    @Override
    public void onBindViewHolder(AbstractViewHolder<TData> holder, int position) {
        if (isDataPosition(position)) {
            holder.bindItem(getItem(position));
        }
    }

    /** determines is position belongs to data set to bindHolder*/
    @SuppressWarnings("WeakerAccess")
    boolean isDataPosition(int position) {
        return true;
    }

    protected Context getContext() {
        return context;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public TData getItem(final int position) throws ArrayIndexOutOfBoundsException {
        return data.get(position);
    }

    public List<TData> getSnapshot() {
        return new ArrayList<>(data);
    }

    public boolean add(TData object) {
        return data.add(object);
    }

    public boolean remove(TData object) {
        return data.remove(object);
    }

    public TData remove(int position) {
        return data.remove(position);
    }

    public void clear() {
        data.clear();
    }

    public void addAll(@NonNull List<? extends TData> collection) {
        data.addAll(collection);
    }

    public int getItemPosition(TData object) {
        return data.indexOf(object);
    }

    public void insert(TData object, int position) {
        data.add(position, object);
    }

    public void insertAll(Collection<? extends TData> object, int position) {
        data.addAll(position, object);
    }
}
