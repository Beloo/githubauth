package com.example.beloo.githubauth.data.network.pojo;

import com.example.beloo.githubauth.data.contract.Repository;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "stargazers_count",
        "updated_at"
})
public class RepositoryPoJo implements Repository {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("stargazers_count")
    private Integer stargazersCount;
    @JsonProperty("updated_at")
    private Date updatedAt;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Integer getStargazersCount() {
        return stargazersCount;
    }

    @Override
    public Date getUpdatedAt() {
        return updatedAt;
    }

}