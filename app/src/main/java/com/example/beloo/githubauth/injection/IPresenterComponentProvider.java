package com.example.beloo.githubauth.injection;

import android.support.v4.app.LoaderManager;

import com.example.beloo.githubauth.support.Consumer;

public interface IPresenterComponentProvider {
    void init(LoaderManager loaderManager, Consumer<PresenterComponent> presenterConsumer);
}
