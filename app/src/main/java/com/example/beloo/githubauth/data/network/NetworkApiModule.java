package com.example.beloo.githubauth.data.network;

import android.content.Context;
import android.os.Build;

import com.example.beloo.githubauth.BuildConfig;
import com.example.beloo.githubauth.data.network.api.AuthApi;
import com.example.beloo.githubauth.data.network.api.RepositoryApi;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter.Factory;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

@Module
public class NetworkApiModule {

    private static final String API_GITHUB = "github";

    @Provides
    OkHttpClient provideHttpClient(Context context) {

        // Enable caching for OkHttp
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(context.getCacheDir(), cacheSize);

        OkHttpClient.Builder builder = new OkHttpClient().newBuilder()
                .cache(cache)
                .connectTimeout(30, TimeUnit.SECONDS);

        builder.addInterceptor(chain -> {
            Request request = chain.request().newBuilder()
                    .addHeader(ServerContract.Headers.CONTENT_TYPE, ServerContract.Headers.APPLICATION_JSON)
                    .addHeader(ServerContract.Headers.ACCEPT, ServerContract.Headers.APPLICATION_JSON)
                    .build();
            return chain.proceed(request);
        });

        //logging interceptor should be last interceptor in chain
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            builder.addInterceptor(loggingInterceptor);
        }

//        builder.addInterceptor(new StethoInterceptor());

        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        builder.cookieJar(new JavaNetCookieJar(cookieManager));

        return builder.build();
    }

    @Singleton
    @Provides
    @Named(API_GITHUB)
    Retrofit provideGithubRetrofitAdapter(OkHttpClient client, Factory factory) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.GITHUB_ENDPOINT)
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(factory)
                .build();
    }

    @Singleton
    @Provides
    Retrofit provideRetrofitAdapter(OkHttpClient client, Factory factory) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.GITHUB_API_ENDPOINT)
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(factory)
                .build();
    }

    @Provides
    AuthApi provideAuthApi(@Named(API_GITHUB) Retrofit retrofit) {
        return retrofit.create(AuthApi.class);
    }

    @Provides
    RepositoryApi provideRepositoryApi(Retrofit retrofit) {
        return retrofit.create(RepositoryApi.class);
    }

}
