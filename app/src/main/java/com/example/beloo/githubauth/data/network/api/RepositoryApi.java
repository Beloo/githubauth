package com.example.beloo.githubauth.data.network.api;

import com.example.beloo.githubauth.data.network.ServerContract;
import com.example.beloo.githubauth.data.network.pojo.RepositoryPoJo;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface RepositoryApi {

    /** get list of all user repositories*/
    @GET(ServerContract.Repository.GET_REPOSITORIES)
    Observable<List<RepositoryPoJo>> getUserRepositories(@Query("access_token") String accessToken);

}
