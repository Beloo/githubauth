package com.example.beloo.githubauth.data.network.source;

import android.support.annotation.VisibleForTesting;

import rx.Observable;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

class SchedulersNetworkProvider {

    private final Observable.Transformer<?, ?> schedulersTransformer =
            observable -> observable.subscribeOn(Schedulers.io())
                    .observeOn(provideScheduler())
                    .doOnError(Throwable::printStackTrace);

    /** @return transformer with which input observable will be updated with io scheduler and observed on main thread
     * Chain will go on the main thread after applying!*/
    @SuppressWarnings("unchecked")
    <T> Observable.Transformer<T, T> applySchedulers() {
        return (Observable.Transformer<T, T>) schedulersTransformer;
    }

    @VisibleForTesting
    Scheduler provideScheduler() {
        return AndroidSchedulers.mainThread();
    }

}
