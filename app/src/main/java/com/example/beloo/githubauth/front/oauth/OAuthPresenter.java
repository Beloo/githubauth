package com.example.beloo.githubauth.front.oauth;

import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.beloo.githubauth.data.contract.AccessToken;
import com.example.beloo.githubauth.data.local.UserDataSource;
import com.example.beloo.githubauth.data.network.ServerContract;
import com.example.beloo.githubauth.data.network.source.AuthSource;
import com.example.beloo.githubauth.front.AbstractPresenter;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;

public class OAuthPresenter extends AbstractPresenter<OauthScreenContract.OAuthView> implements OauthScreenContract.OAuthPresenter {

    private static final String TAG = OAuthPresenter.class.getSimpleName();

    @Inject
    AuthSource authSource;

    @Inject
    UserDataSource userDataSource;

    @Nullable
    private Observable<AccessToken> accessTokenObservable;

    private Subscription urlSubscription;

    @Inject
    OAuthPresenter(){}

    @Override
    protected void onReady() {
        if (accessTokenObservable != null) {
            //continue listening if available
            receiveToken();
        } else {
            urlSubscription = getView().loadUrl(ServerContract.Auth.getOauthToken())
                    .filter(url -> url.startsWith(ServerContract.Auth.REGISTERED_REDIRECT_URL))
                    .map(url -> {
                        Log.d(TAG, "on redirect url received (successful user data)");
                        Uri uri = Uri.parse(url);
                        return uri.getQueryParameter(ServerContract.Auth.CODE);
                    })
                    .subscribe(this::onStartLoadingPage, getView()::showError);
            registerSubscription(urlSubscription);
        }
    }

    private void receiveToken() {
        getView().stopLoading();
        getView().showProgress();
        @SuppressWarnings("ConstantConditions") Subscription subscription = accessTokenObservable
                .subscribe(this::onAccessTokenReceived, this::onAccessTokenError);

        registerSubscription(subscription);
    }

    private void onStartLoadingPage(String code) {
        urlSubscription.unsubscribe();
        accessTokenObservable = authSource.getAccessToken(code).cache();
        receiveToken();
    }

    private void onAccessTokenReceived(AccessToken accessToken) {
        Log.d(TAG, "on access token received");
        accessTokenObservable = null;
        getView().hideProgress();
        userDataSource.storeAccessToken(accessToken);
        getView().onLoginSuccessful();
    }

    private void onAccessTokenError(Throwable error) {
        Log.d(TAG, "on access token receiving error");
        accessTokenObservable = null;
        getView().hideProgress();
        getView().showError(error);
        getView().onLoginCancelled();
    }

}
