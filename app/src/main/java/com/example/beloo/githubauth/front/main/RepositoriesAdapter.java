package com.example.beloo.githubauth.front.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.beloo.githubauth.R;
import com.example.beloo.githubauth.data.contract.Repository;
import com.example.beloo.githubauth.front.AbstractViewHolder;
import com.example.beloo.githubauth.front.BaseRecyclerViewAdapter;

import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.BindView;

class RepositoriesAdapter extends BaseRecyclerViewAdapter<Repository> {

    RepositoriesAdapter(@NonNull Context context) {
        super(context);
    }

    @Override
    public AbstractViewHolder<Repository> onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repository, parent, false);
        return new RepositoryViewHolder(itemView);
    }

    @SuppressWarnings("WeakerAccess")
    static class RepositoryViewHolder extends AbstractViewHolder<Repository> {

        @BindView(R.id.tvRepositoryName)
        TextView tvRepositoryName;
        @BindView(R.id.tvUpdateDate)
        TextView tvUpdateDate;
        @BindView(R.id.tvStarCount)
        TextView tvStarCount;

        RepositoryViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void bindItem(Repository item) {
            tvRepositoryName.setText(item.getName());

            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
            tvUpdateDate.setText(format.format(item.getUpdatedAt()));

            tvStarCount.setText(String.valueOf(item.getStargazersCount()));
        }
    }
}
