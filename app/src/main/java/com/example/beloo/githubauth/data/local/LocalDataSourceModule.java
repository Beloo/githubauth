package com.example.beloo.githubauth.data.local;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LocalDataSourceModule {

    @Provides
    @Singleton
    UserDataSource provideUserDataSource(UserDataPrefs prefs) {
        return prefs;
    }

}
