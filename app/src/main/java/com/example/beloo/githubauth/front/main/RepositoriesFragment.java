package com.example.beloo.githubauth.front.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.beloo.githubauth.R;
import com.example.beloo.githubauth.data.contract.Repository;
import com.example.beloo.githubauth.front.ViewFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class RepositoriesFragment extends ViewFragment implements RepositoriesScreenContract.RepositoriesView {

    @BindView(R.id.rvRepositories)
    RecyclerView rvRepositories;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Inject
    RepositoriesScreenContract.RepositoriesPresenter presenter;

    private RepositoriesAdapter repositoriesAdapter;

    @Override
    public int getLayoutResId() {
        return R.layout.fragment_repositories;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvRepositories.setLayoutManager(new LinearLayoutManager(getContext()));
        repositoriesAdapter = new RepositoriesAdapter(getContext());
        rvRepositories.setAdapter(repositoriesAdapter);
    }

    @Override
    protected void onComponentsInitialized() {
        getPresenterComponent().inject(this);
        presenter.bindView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.unbindView();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onDataLoaded(List<Repository> items) {
        repositoriesAdapter.clear();
        repositoriesAdapter.addAll(items);
        repositoriesAdapter.notifyDataSetChanged();
    }
}
