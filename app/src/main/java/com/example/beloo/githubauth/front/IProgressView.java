package com.example.beloo.githubauth.front;

public interface IProgressView {
    void showProgress();
    void hideProgress();
}
