package com.example.beloo.githubauth.injection;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;

@Module
class GlobalModule {

    private Context appContext;

    public GlobalModule(@NonNull Context context) {
        appContext = context;
    }

    @Provides
    Context provideAppContext() {
        return appContext;
    }

    @Provides
    AssetManager provideAssetsManager(Context context) {
        return context.getAssets();
    }

    @Provides
    ContentResolver provideContentResolver(Context context) {
        return context.getContentResolver();
    }

    @Provides
    Resources provideResources(Context context) {
        return context.getResources();
    }

    @Provides
    IPresenterComponentProvider providePresenterComponentProvider(PresenterComponentProvider provider) {
        return provider;
    }
}
