package com.example.beloo.githubauth.front;

import android.support.annotation.StringRes;
import android.support.v4.app.FragmentActivity;

public interface IView {
    /** don't store activity in strong references!*/
    FragmentActivity getActivity();
    void showError(Throwable throwable);
    void showError(@StringRes int stringResource);
}
