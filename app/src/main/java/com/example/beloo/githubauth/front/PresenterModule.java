package com.example.beloo.githubauth.front;

import com.example.beloo.githubauth.front.login.LoginPresenter;
import com.example.beloo.githubauth.front.login.LoginScreenContract;
import com.example.beloo.githubauth.front.main.RepositoriesPresenter;
import com.example.beloo.githubauth.front.main.RepositoriesScreenContract;
import com.example.beloo.githubauth.front.oauth.OAuthPresenter;
import com.example.beloo.githubauth.front.oauth.OauthScreenContract;
import com.example.beloo.githubauth.injection.PresenterScope;

import dagger.Module;
import dagger.Provides;

@Module
public class PresenterModule {

    @Provides
    @PresenterScope
    OauthScreenContract.OAuthPresenter provideOAuthPresenter(OAuthPresenter presenter) {
        return presenter;
    }

    @Provides
    @PresenterScope
    LoginScreenContract.LoginPresenter provideLoginPresenter(LoginPresenter presenter) {
        return presenter;
    }

    @Provides
    @PresenterScope
    RepositoriesScreenContract.RepositoriesPresenter provideRepositoriesPresenter(RepositoriesPresenter presenter) {
        return presenter;
    }

}
