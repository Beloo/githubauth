package com.example.beloo.githubauth.data.network.source;

import com.example.beloo.githubauth.data.contract.AccessToken;

import rx.Observable;

public interface AuthSource {
    /** @param code is a value from first step of oauth */
    Observable<AccessToken> getAccessToken(String code);
}
