package com.example.beloo.githubauth.data.network.source;

import com.example.beloo.githubauth.BuildConfig;
import com.example.beloo.githubauth.data.contract.AccessToken;
import com.example.beloo.githubauth.data.network.api.AuthApi;
import com.example.beloo.githubauth.data.network.pojo.request.AccessTokenRequest;

import javax.inject.Inject;

import rx.Observable;

class AuthRemoteSource extends SchedulersNetworkProvider implements AuthSource {

    @Inject
    AuthApi authApi;

    @Inject
    AuthRemoteSource() {}

    @Override
    public Observable<AccessToken> getAccessToken(String code) {
        return authApi.getAccessToken(new AccessTokenRequest(BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET, code))
                .cast(AccessToken.class)
                .compose(applySchedulers());
    }
}
