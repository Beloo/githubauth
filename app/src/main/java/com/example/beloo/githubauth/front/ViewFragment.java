package com.example.beloo.githubauth.front;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.beloo.githubauth.GithubAuthApplication;
import com.example.beloo.githubauth.injection.IPresenterComponentProvider;
import com.example.beloo.githubauth.injection.PresenterComponent;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class ViewFragment extends Fragment implements IView {

    private PresenterComponent presenterComponent;
    private View rootView;

    private Unbinder unbinder;

    @Inject
    IPresenterComponentProvider provider;

    public ViewFragment() {
        GithubAuthApplication.instance()
                .componentFactory()
                .globalComponent()
                .inject(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //loaders should be called in onActivityCreated due to google guidelines
        provider.init(getLoaderManager(), presenterComponent -> this.presenterComponent = presenterComponent);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = LayoutInflater.from(getContext()).inflate(getLayoutResId(), container, false);
        unbinder = ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (presenterComponent != null) {
            onComponentsInitialized();
        }
    }

    protected abstract void onComponentsInitialized();

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    @LayoutRes
    public abstract int getLayoutResId();

    public View getRootView() {
        return rootView;
    }

    /** retrieve presenter component and you'll able to inject presenters.
     * this method should be called in {@link Fragment#onResume()} ()} or in {@link Activity#onStart()}, because
     * presenter component loader is working on a main thread and it is ready in that methods.
     * Fragment have a broken loader lifecycle, so getComponent should be called in onResume */
    protected PresenterComponent getPresenterComponent() {
        return presenterComponent;
    }

    @Override
    public void showError(Throwable throwable) {
        Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(@StringRes int stringResource) {
        Toast.makeText(getContext(), stringResource, Toast.LENGTH_LONG).show();
    }
}
