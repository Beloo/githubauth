package com.example.beloo.githubauth;

import android.app.Application;

import com.example.beloo.githubauth.injection.ComponentCreator;
import com.example.beloo.githubauth.injection.ComponentFactory;
import com.example.beloo.githubauth.injection.ComponentProvider;

public class GithubAuthApplication extends Application {

    private static GithubAuthApplication instance;

    private ComponentFactory componentFactory;

    public static GithubAuthApplication instance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        componentFactory = new ComponentProvider(new ComponentCreator(this));
    }

    public ComponentFactory componentFactory() {
        return componentFactory;
    }
}
