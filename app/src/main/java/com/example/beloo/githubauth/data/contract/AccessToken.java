package com.example.beloo.githubauth.data.contract;

public interface AccessToken {
    String getAccessToken();

    String getScope();

    String getTokenType();
}
