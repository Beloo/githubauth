package com.example.beloo.githubauth.data.network.pojo;

import com.example.beloo.githubauth.data.contract.AccessToken;

import javax.inject.Inject;

public class AccessTokenFactory {

    @Inject
    public AccessTokenFactory(){}

    public AccessToken buildAccessToken(String token, String scope, String type) {
        return new AccessTokenPoJo(token, scope, type);
    }
}
