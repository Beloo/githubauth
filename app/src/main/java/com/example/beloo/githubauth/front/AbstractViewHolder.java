package com.example.beloo.githubauth.front;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.lang.ref.WeakReference;

import butterknife.ButterKnife;

public abstract class AbstractViewHolder<T> extends RecyclerView.ViewHolder {

    private View itemView;
    private WeakReference<OnItemClickListener> onItemClickListener;

    public AbstractViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.itemView = itemView;
    }

    protected View getItemView() {
        return itemView;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        if (onItemClickListener != null) {
            this.onItemClickListener = new WeakReference<>(onItemClickListener);
            getItemView().setOnClickListener(v -> {
                if (this.onItemClickListener.get()!= null && getAdapterPosition() != -1) {
                    this.onItemClickListener.get().onItemClicked(getAdapterPosition());
                }
            });
        } else {
            this.onItemClickListener = null;
        }
    }

    public abstract void bindItem(T item);

    protected Context getContext() {
        if (itemView!=null) {
            return itemView.getContext();
        }
        return null;
    }
}
