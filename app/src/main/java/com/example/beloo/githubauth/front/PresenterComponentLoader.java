package com.example.beloo.githubauth.front;

import android.content.Context;
import android.support.v4.content.Loader;

import com.example.beloo.githubauth.GithubAuthApplication;
import com.example.beloo.githubauth.injection.ComponentFactory;
import com.example.beloo.githubauth.injection.PresenterComponent;


public class PresenterComponentLoader extends Loader<PresenterComponent> {
    private PresenterComponent presenterComponent;
    private ComponentFactory componentFactory;

    /**
     * Stores away the application context associated with context.
     * Since Loaders can be used across multiple activities it's dangerous to
     * store the context directly; always use {@link #getContext()} to retrieve
     * the Loader's Context, don't use the constructor argument directly.
     * The Context returned by {@link #getContext} is safe to use across
     * Activity instances.
     *
     * @param context used to retrieve the application context.
     */

    public PresenterComponentLoader(Context context) {
        super(context);
        componentFactory = GithubAuthApplication.instance().componentFactory();
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();

        //deliver presenter component if cached
        if (presenterComponent != null) {
            deliverResult(presenterComponent);
            return;
        }

        //load it otherwise
        forceLoad();
    }

    @Override
    protected void onForceLoad() {
        super.onForceLoad();

        //create presenter component using global factory
        presenterComponent = componentFactory.presenterComponent();

        //deliver component
        deliverResult(presenterComponent);
    }

    @Override
    protected void onReset() {
        super.onReset();
        //ensure that loader is stopped
        onStopLoading();

        //free resources
        presenterComponent = null;
    }
}
