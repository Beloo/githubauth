package com.example.beloo.githubauth.front.main;

import com.example.beloo.githubauth.data.contract.Repository;
import com.example.beloo.githubauth.front.IPresenter;
import com.example.beloo.githubauth.front.IProgressView;
import com.example.beloo.githubauth.front.IView;

import java.util.List;

public interface RepositoriesScreenContract {

    interface RepositoriesView extends IView, IProgressView {
        void onDataLoaded(List<Repository> items);
    }

    interface RepositoriesPresenter extends IPresenter<RepositoriesView> {

    }

}
