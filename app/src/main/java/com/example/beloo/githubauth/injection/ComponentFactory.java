package com.example.beloo.githubauth.injection;

import android.content.Context;

public interface ComponentFactory {

    GlobalComponent globalComponent();

    PresenterComponent presenterComponent();

    Context getAppContext();

}
