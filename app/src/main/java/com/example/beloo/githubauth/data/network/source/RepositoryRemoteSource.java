package com.example.beloo.githubauth.data.network.source;

import com.example.beloo.githubauth.data.contract.Repository;
import com.example.beloo.githubauth.data.local.UserDataSource;
import com.example.beloo.githubauth.data.network.api.RepositoryApi;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

class RepositoryRemoteSource extends SchedulersNetworkProvider implements RepositorySource {

    @Inject
    RepositoryApi repositoryApi;

    @Inject
    UserDataSource userDataSource;

    @Inject
    RepositoryRemoteSource(){}

    @Override
    public Observable<List<Repository>> getRepositories() {
        if (userDataSource.getAccessTokenHolder() == null) return Observable.error(new IllegalStateException("token haven't stored"));
        return repositoryApi.getUserRepositories(userDataSource.getAccessTokenHolder().getAccessToken())
                .flatMap(Observable::from)
                .cast(Repository.class)
                .toList()
                .compose(applySchedulers());
    }

}
