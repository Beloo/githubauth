package com.example.beloo.githubauth.front;

public interface IPresenter<View> {
    void bindView(View view);
    void unbindView();
}
