package com.example.beloo.githubauth.injection;

public class ComponentProvider extends ComponentCreator implements ComponentFactory {

    private ComponentFactory componentFactory;
    private GlobalComponent globalComponent;

    public ComponentProvider(ComponentFactory componentFactory) {
        super(componentFactory.getAppContext());
        this.componentFactory = componentFactory;
    }

    @Override
    public GlobalComponent globalComponent() {
        if (globalComponent == null) {
            globalComponent = componentFactory.globalComponent();
        }
        return globalComponent;
    }

}
