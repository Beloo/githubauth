package com.example.beloo.githubauth.data.network.api;

import com.example.beloo.githubauth.data.network.ServerContract;
import com.example.beloo.githubauth.data.network.pojo.AccessTokenPoJo;
import com.example.beloo.githubauth.data.network.pojo.request.AccessTokenRequest;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;


public interface AuthApi {

    @POST(ServerContract.Auth.POST_OAUTH_TOKEN)
    Observable<AccessTokenPoJo> getAccessToken(@Body AccessTokenRequest accessToken);

}
