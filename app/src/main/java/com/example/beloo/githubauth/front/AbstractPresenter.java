package com.example.beloo.githubauth.front;

import android.support.v4.app.FragmentActivity;

import java.lang.ref.WeakReference;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public abstract class AbstractPresenter <View extends IView> implements IPresenter<View> {
    private WeakReference<View> view;
    private WeakReference<FragmentActivity> weakActivity;
    private CompositeSubscription compositeSubscription;

    public View getView() {
        return view.get();
    }

    public void registerSubscription(Subscription... subscriptions) {
        for (Subscription subscription : subscriptions) {
            compositeSubscription.add(subscription);
        }
    }

    protected abstract void onReady();

    FragmentActivity getActivity() {
        checkViewAttachedAndThrow();
        if (weakActivity == null || weakActivity.get() == null) throw new IllegalStateException("activity is null until onReady executed!");
        return weakActivity.get();
    }


    @Override
    public final void bindView(View view) {
        this.view = new WeakReference<>(view);
        if (view.getActivity() != null) {
            weakActivity = new WeakReference<>(view.getActivity());
        }
        compositeSubscription = new CompositeSubscription();
        onReady();
    }

    @Override
    public void unbindView() {
        this.view = null;

        //unsubscribe from all registered subscriptions when android destroys view
        compositeSubscription.unsubscribe();
    }

    @SuppressWarnings("WeakerAccess")
    protected boolean isViewAttached() {
        return view != null;
    }

    void checkViewAttachedAndThrow() {
        if (!isViewAttached()) throw new MVPViewNotAttachedException();
    }

    void onError(Throwable throwable) {
        getView().showError(throwable);
    }

    private static final class MVPViewNotAttachedException extends RuntimeException {
        MVPViewNotAttachedException() {
            super("Presenter's #bindView() have to be called before");
        }
    }
}